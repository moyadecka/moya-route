import * as firebase from 'firebase'
import { toast } from '../Toast/toast'
require('dotenv/config');

const config={

        apiKey: `${process.env.REACT_APP_API_KEY}`,
        authDomain: "moya-route.firebaseapp.com",
        databaseURL: "https://moya-route.firebaseio.com",
        projectId: "moya-route",
        storageBucket: "moya-route.appspot.com",
        messagingSenderId: "342608328529",
        appId: "1:342608328529:web:8af2969b4d8172141f0570",
        measurementId: "G-HV2JTVS7GP"
      
}
firebase.initializeApp(config)

export function getCurrentUser(){
    return new Promise((resolve, reject)=>{
        const unsubscribe = firebase.auth().onAuthStateChanged(function(user){
            if(user){
                resolve(user)
                unsubscribe()
            } else{
                resolve(null)
            }
        })
    })
}
export function logoutUser(){
    return firebase.auth().signOut()
}
export async function loginUser(username:string,password:string){
    const email = `${username}@example.com`
    try{
        const res = await firebase.auth().signInWithEmailAndPassword(email,password)
        console.log(res)
        return res        
    }
    catch(error){
        //console.log(error)
        toast(error.message,4000)
        return false
    }
}
export async function registerUser(username:string,password:string){
    const email = `${username}@example.com`
    try{
        const res = await firebase.auth().createUserWithEmailAndPassword(email,password)
        console.log(res)
        return true
    }catch (error){
        //console.log(error)
        toast(error.message,4000)
        return false
    }
}