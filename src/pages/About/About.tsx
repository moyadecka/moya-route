import React, { Component } from 'react';

import {
  IonContent,
  IonCard,
  IonCardHeader,
  IonCardTitle,
  IonCardSubtitle
} from '@ionic/react';
import Typed from 'react-typed';
import 'react-typed/dist/animatedCursor.css';
  
class About extends Component {
  render() {
    return (
      <IonContent>
          <IonCard>
            <IonCardHeader>
              <IonCardTitle>We are..  
                <Typed style={{color: '#eb445a'}}
                    strings={[
                      ' people',
                      ' black',
                      ' white',
                      ' poor',
                      ' rich',
                      ' gay',
                      ' straight',
                      ' professionals',
                      ' enthusiasts',
                      ' the same!']}
                      typeSpeed={40}
                      backSpeed={50} 
                      loop                     
                  />
                </IonCardTitle>
                <IonCardSubtitle><Typed 
                    strings={[
                      'Welcome to Bike4Fun.']}
                      typeSpeed={40}       
                  />
                  </IonCardSubtitle>
            </IonCardHeader>
          </IonCard>
          <IonCard>
            <IonCardHeader>
            <IonCardTitle>  
                <Typed 
                    strings={[
                      'All cyclists around the world can connect via this platform, try existing bike routes or propose their favorite one.']}
                      typeSpeed={40}       
                  />
                </IonCardTitle>
              <IonCardSubtitle>
              <Typed 
                    strings={[
                      'We care for your well being.']}
                      typeSpeed={40}               
                  />
              </IonCardSubtitle>
            </IonCardHeader>
          </IonCard>            
          <IonCard>
            <IonCardHeader>
            <IonCardTitle>Is your route..
                <Typed style={{color: '#ffc409'}}
                    strings={[
                      'cohesive?',
                      'direct?',
                      'safe?',
                      'pleasant?',
                      'attractive?']}
                      typeSpeed={40}
                      backSpeed={50}     
                      loop                 
                  />
                </IonCardTitle>
              <IonCardSubtitle>
              <Typed 
                    strings={[
                      "Fill in the surveys that you'll find inside this app and shape your future!"]}
                      typeSpeed={40}             
                  />
              </IonCardSubtitle>
            </IonCardHeader>            
          </IonCard> 
          <IonCard>
            <IonCardHeader>
            <IonCardTitle >
                <Typed 
                    strings={[
                      'Cohesion: “ You can cycle from anywhere to everywhere ” ',
                      'Directness: “ Short and fast routes ”',
                      'Safety: “ (Traffic) Health and Road Safety ”',
                      'Comfort: “ Avoid traffic nuisance, limiting stops, optimizing wayfinding, enjoyable ride ”',
                      'Attractiveness: “ Open spaces, water, clean air, street art ”']}
                      typeSpeed={40}
                      backSpeed={50}     
                      loop                 
                  />
                </IonCardTitle>
              <IonCardSubtitle>
              <Typed style={{color: '#ffc409'}}
                    strings={[
                      "&copy; DTV Capacity Building"]}
                      typeSpeed={40}                      
                  />
              </IonCardSubtitle>
            </IonCardHeader>
          </IonCard>  
        </IonContent>
    );
  }
}


export default About;

