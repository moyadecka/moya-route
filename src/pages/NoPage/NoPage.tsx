import React, { Component } from 'react';

import {
  
  IonContent,
  IonCard,
  IonCardHeader,
  IonCardTitle,
} from '@ionic/react';

class NoPage extends Component {
  
  render() {
    
    return (
      
        <IonContent>
          <IonCard>
            <IonCardHeader>
              <IonCardTitle>Error 404 Page Not Found</IonCardTitle>
            </IonCardHeader>
          </IonCard>
          
      
        
        </IonContent>
     
    );
  }
}

export default NoPage;

