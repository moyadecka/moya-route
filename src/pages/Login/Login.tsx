import './Login.scss';
import history from '../../History/History';
import React,{ useState} from 'react';

import {
  IonContent,
  IonButton,
  IonItem,
  IonGrid,
  IonCol,
  IonInput,
  IonRow,
  IonLoading
} from '@ionic/react';
import { Link } from 'react-router-dom';
import {loginUser} from '../../Firebase/firebaseConfig'
import { toast } from '../../Toast/toast';
import { useDispatch } from 'react-redux';
import { setUserState} from '../../redux/actions'

const Login: React.FC = () => {
  
    const [busy,setBusy] = useState<boolean>(false)
    const dispatch = useDispatch()
    const [username,setUsername] = useState('')
    const [password, setPassword] = useState('')
    async function login(){
      setBusy(true)
      const res: any = await loginUser(username,password)
      //console.log(`${res ? 'Login success' : 'Login failed'}`)
      if(res){
        dispatch(setUserState(res.user.email))
        history.push('/geolocation')
        toast('You have logged in!')

      }
      setBusy(false)
    }
    return (
      
        <IonContent className="ion-padding" fullscreen >  
        <IonLoading message = "Please wait.." duration={0}
         isOpen = {busy} />       
          <IonGrid>
            <IonRow class="ion-align-items-center">
              
              <svg className="black-blob"viewBox="0 0 200 200" xmlns="http://www.w3.org/2000/svg">
                <path fill="#F1C21B" d="M65.6,-0.4C65.6,26.7,32.8,53.5,0.2,53.5C-32.4,53.5,-64.9,26.7,-64.9,-0.4C-64.9,-27.6,-32.4,-55.2,0.2,-55.2C32.8,-55.2,65.6,-27.6,65.6,-0.4Z" transform="translate(100 100)" />
              </svg>
              
              <svg className="black-blob3"viewBox="0 0 200 200" xmlns="http://www.w3.org/2000/svg">
                <path fill="#24A148" d="M24.8,15.4C22.7,17.8,5.3,12.4,-1.5,7.3C-8.2,2.2,-4.1,-2.7,4.7,0C13.4,2.6,26.8,12.9,24.8,15.4Z" transform="translate(100 100)" />
              </svg>
              <svg className="black-blob4"viewBox="0 0 200 200" xmlns="http://www.w3.org/2000/svg">
                <path fill="#24A148" d="M21.8,20.9C7.8,36.9,-38.2,42.7,-47.1,29.7C-56.1,16.6,-28.1,-15.5,-5.1,-18.4C17.9,-21.3,35.9,4.9,21.8,20.9Z" transform="translate(100 100)" />
              </svg>
             <IonCol size='12' class="ion-text-center">
                <IonItem class="ion-text-center" lines="none" id="projectTitle" >
                  <IonInput id="title" class="text-input" placeholder="Login Panel"></IonInput>
                </IonItem>
              </IonCol>
            </IonRow>
            <IonRow>
              <IonCol  class="ion-text-center">
                <IonInput style={{ background:'white', opacity:'0.8'}} id = "username" class="text-input" placeholder="Username" onIonChange={(e: any) => setUsername(e.target.value)}></IonInput>
              </IonCol> 
              <IonCol size="12" class="ion-text-center"> 
                <IonInput 
                type="password" 
                style={{ background:'white', opacity:'0.8'}} id="password" class="text-input" placeholder="Password"
                onIonChange={(e: any) => setPassword(e.target.value)}></IonInput>
              </IonCol>
            </IonRow>
            <IonRow>
              <IonCol size="12" class="ion-text-center">
                <IonButton   color="warning" onClick={login}>Login</IonButton>
                </IonCol>
                <IonCol size="12" class="ion-text-center">
                  <p>New here? <Link to="/register" style= {{color:'#ffc409'}}> Register</Link></p>
                </IonCol>
              
            </IonRow>
          </IonGrid>
          


        </IonContent>
        
         
    
    );
  }


export default Login;

