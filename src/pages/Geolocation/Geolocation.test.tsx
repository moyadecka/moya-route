import React from 'react';
import { render } from '@testing-library/react';
import Geolocation from './Geolocation';

test('renders learn react link', () => {
  const { getByText } = render(<Geolocation />);
  const linkElement = getByText(/learn react/i);
  expect(linkElement).toBeInTheDocument();
});
