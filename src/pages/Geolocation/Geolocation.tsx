import React, { Component, useState } from 'react';
import './Geolocation.scss';
import history from '../../History/History';
import {
  IonContent,
  IonButton,
  IonCol,
  IonGrid,
  IonRow,
  IonLoading
} from '@ionic/react';
import { useSelector} from 'react-redux';
import { logoutUser} from '../../Firebase/firebaseConfig'
const Geolocation : React.FC = () => {
  
      const username = useSelector((state:any)=>state.user.username)
      const [busy,setBusy]=useState(false) 
      async function logout(){
        setBusy(true)
        await logoutUser()
        setBusy(false)
        history.push('/')
      }
      return (
          <IonContent className="ion-padding" fullscreen >
          <IonGrid>
              <IonRow class="ion-align-items-center">
              <svg className="black-blob"viewBox="0 0 200 200" xmlns="http://www.w3.org/2000/svg">
                  <path fill="#F1C21B" d="M65.6,-0.4C65.6,26.7,32.8,53.5,0.2,53.5C-32.4,53.5,-64.9,26.7,-64.9,-0.4C-64.9,-27.6,-32.4,-55.2,0.2,-55.2C32.8,-55.2,65.6,-27.6,65.6,-0.4Z" transform="translate(100 100)" />
                </svg>
                <svg className="black-blob2"viewBox="0 0 200 200" xmlns="http://www.w3.org/2000/svg">
                  <path fill="#0F62FE" d="M38.7,-4.1C38.7,24.5,19.3,48.9,4.7,48.9C-10,48.9,-20,24.5,-20,-4.1C-20,-32.7,-10,-65.3,4.7,-65.3C19.3,-65.3,38.7,-32.7,38.7,-4.1Z" transform="translate(100 100)" />
                </svg>
                <svg className="black-blob3"viewBox="0 0 200 200" xmlns="http://www.w3.org/2000/svg">
                  <path fill="#24A148" d="M24.8,15.4C22.7,17.8,5.3,12.4,-1.5,7.3C-8.2,2.2,-4.1,-2.7,4.7,0C13.4,2.6,26.8,12.9,24.8,15.4Z" transform="translate(100 100)" />
                </svg>
                <svg className="black-blob4"viewBox="0 0 200 200" xmlns="http://www.w3.org/2000/svg">
                  <path fill="#24A148" d="M21.8,20.9C7.8,36.9,-38.2,42.7,-47.1,29.7C-56.1,16.6,-28.1,-15.5,-5.1,-18.4C17.9,-21.3,35.9,4.9,21.8,20.9Z" transform="translate(100 100)" />
                </svg>
                <IonCol size='12' class="ion-text-center">
                <IonLoading message = "Please wait.." duration={0}
                  isOpen = {busy} /> 
                <p>Hello {username}</p>
              <IonButton onClick={logout}>Logout</IonButton>
                </IonCol>
                
              </IonRow>
            </IonGrid>
        </IonContent>   
      ); 
  
}

export default Geolocation;
