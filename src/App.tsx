import '@ionic/core/css/core.css';
import '@ionic/core/css/ionic.bundle.css';
import { 
   IonApp,
   IonButton,
   IonButtons, 
   IonContent, 
   IonHeader, 
   IonItem, 
   IonList, 
   IonMenu, 
   IonMenuButton, 
   IonPage, 
   IonRouterOutlet, 
   IonSpinner, 
   IonTitle, 
   IonToolbar
} from '@ionic/react';
import Footer from './components/Footer/Footer';
import Header from './components/Header/Header';
import React, { useState, useEffect } from 'react';
import Routes from './routes';
import { menuController } from '@ionic/core';
import history from './History/History';
import  { logoutUser, getCurrentUser} from './Firebase/firebaseConfig'
import { useDispatch } from 'react-redux';
import { setUserState } from './redux/actions';

const App: React.FC=() =>{
  const [busy,setBusy] = useState(true)
  const dispatch = useDispatch()
    useEffect(() =>{
      getCurrentUser().then((user:any) =>{
        console.log(user)
        if(user){
          dispatch(setUserState(user.email))
          history.push('/geolocation')
        } else{
          history.push('/')
        }
        setBusy(false)
      })
    }, [])
  return <IonApp>{busy ? <IonSpinner/>:<RoutingSystem/>}</IonApp>
} 


const RoutingSystem: React.FC = () =>{
  const [menu,setMenu] = useState(false)
  

  const dispatch = useDispatch()
  
  async function item1(){
    
      getCurrentUser().then((user:any) =>{
        if(user){
          dispatch(setUserState(user.email))
          setMenu(true)       
          history.push('/geolocation')
        } else{
          setMenu(false)    
          history.push('/')
        }
      })  
  }
  async function item2(){
      getCurrentUser().then((user:any) =>{
        if(user){
          dispatch(setUserState(user.email))
          setMenu(true)
          history.push('/bl')
        } else{
          setMenu(false)    
          history.push('/about')
        }
      })
  }
  const [busy,setBusy]=useState(false)
    
    async function logout(){
      setBusy(true)
      await logoutUser()
      setBusy(false)
      history.push('/')
    }
  async function item3(){   
      getCurrentUser().then((user:any) =>{
        if(user){
          dispatch(setUserState(user.email))
          setMenu(true) 
          logout()
          
        } else{
          setMenu(false) 
          history.push('/settings')
        }
      }) 
      
  }
  
  return(
    <IonApp>
        <IonMenu side="end" contentId="main" type="overlay">
        <Header/> 
        <IonContent>
        <IonList>
          <IonItem><IonButton onClick={item1}color='warning' >{menu ? 'Track' : 'Home'}</IonButton></IonItem>
          <IonItem><IonButton onClick={item2}color='warning' >{menu ? 'FindOthers': 'About'}</IonButton></IonItem>
          <IonItem><IonButton onClick={item3}color='warning' >{menu ? 'Logout' : 'Settings'}</IonButton></IonItem>
        </IonList>
      </IonContent>
        </IonMenu>
        <IonPage id="main"/>
        <IonHeader>
      <IonToolbar>
        <IonButtons slot="end">
        <IonMenuButton autoHide={false} 
          onClick={() => menuController.open()}>
        </IonMenuButton>
        </IonButtons>
        <IonTitle class="ion-text-center">Bike4Fun</IonTitle>
      </IonToolbar>
    </IonHeader>
    <IonRouterOutlet id="main"></IonRouterOutlet>
        <Routes/>
        <Footer/>
      </IonApp>
  )
}
export default App      
      

