import React, { Component } from 'react';
import './Footer.scss';

import {
  IonTitle,
  IonToolbar,
  IonFooter
} from '@ionic/react';
import Typed from 'react-typed';

class Footer extends Component {
  
  render() {
    
    return (
      <IonFooter>
      <IonToolbar><h4 className="ion-text-center">
                <Typed
                    strings={[
                      'You wanna contribute?',
                      'Send me an email at',
                      'simple@example.com']}
                      typeSpeed={90}
                      backSpeed={30} 
                                          
                  />
                </h4>
        
        <IonTitle class="ion-text-center">&copy; 2020 Moya Ionic</IonTitle>
      </IonToolbar>

      
    </IonFooter>

    

    
    );
  }
}

export default Footer;

