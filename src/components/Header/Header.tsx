import React, { Component } from 'react';
import './Header.scss';
import {
  IonTitle,
  IonToolbar,
  IonHeader
} from '@ionic/react';

class Header extends Component {
  
  render() {
    
    return (
      <IonHeader class="ion-no-border">
        
      <IonToolbar >
        
        
        <IonTitle class="ion-text-center">Bike4Fun</IonTitle>
        
      </IonToolbar> 
      
      
      
    </IonHeader>

    
    );
  }
}

export default Header;

