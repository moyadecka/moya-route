import {Router, Route, Switch} from 'react-router-dom';
import Home from './pages/Home/Home';
import About from './pages/About/About';
import Settings from './pages/Settings/Settings';
import NoPage from './pages/NoPage/NoPage';
import React from 'react';
import history from './History/History';
import Login from './pages/Login/Login';
import Register from './pages/Register/Register';
import Geolocation from './pages/Geolocation/Geolocation';

const Routes = () => (
    <Router history={history}>
    <Switch>
        <Route exact path='/' component={Home}/>
        <Route exact path='/about' component={About}/>
        <Route exact path='/settings' component={Settings}/>
        <Route exact path='/login' component={Login}/>
        <Route exact path='/register' component={Register}/>
        <Route exact path='/geolocation' component={Geolocation}/>
        <Route exact path='*' component={NoPage}/>
    </Switch>
    </Router>
);
export default Routes;
